﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace MiniManager
{
    public partial class ManagerForm : Form
    {
        public ManagerForm()
        {
            InitializeComponent();
        }

        public void SetPanels(Panel[] panels)
        {
            if (panels[0] != null)
            {
                splitPanelContainer.Panel1.Controls.Add(panels[0]);
                panels[0].Dock = DockStyle.Fill;
                splitPanelContainer.Panel2.Controls.Add(panels[1]);
                panels[1].Dock = DockStyle.Fill;
            }
        }

        private void menuFont_Click(object sender, EventArgs e)
        {
            var dialog = new FontDialog();
            if(dialog.ShowDialog(this) == DialogResult.OK)
            {
                Settings.Instance.FontSettings = dialog.Font;
            }
        }

        private void menuColor_Click(object sender, EventArgs e)
        {
            var dialog = new ColorDialog();
            if (dialog.ShowDialog(this) == DialogResult.OK)
            {
                Settings.Instance.ColorSettings = dialog.Color;
            }

        }

        private void ManagerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Instance.Serialize();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            PanelController.ActiveInstance.CopyItems();

        }

        private void btnMove_Click(object sender, EventArgs e)
        {
            PanelController.ActiveInstance.MoveItems();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            PanelController.ActiveInstance.RemoveItems();
        }

        private void btnRename_Click(object sender, EventArgs e)
        {
            PanelController.ActiveInstance.RenameItems();
        }

        private void btnAssemblyData_Click(object sender, EventArgs e)
        {
            Panel pnl = PanelController.ActiveInstance.ActivePanel;
            try
            {
                AssemblyDataForm form = new AssemblyDataForm(pnl.SelectedItemsInfo[0].ToString());
                form.Show();
            }
            catch
            {
                MessageBox.Show("No items selected or selected file is not an assembly");
            }
        }
    }
}
