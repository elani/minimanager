﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniManager
{
    public delegate void ResponseDelegate(DirectoryDescription description);
    interface IFileSystemDataSource
    {
        void GetDirectoryContent(string path, ResponseDelegate description);
        void GetRootPath(ResponseDelegate description);
    }
    public class DirectoryDescription
    {
        public string Path { get; set; }
        public List<string> Directories { get; set; }
        public List<string> Files { get; set; }
    }
}
