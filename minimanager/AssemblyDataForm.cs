﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace MiniManager
{
    public partial class AssemblyDataForm : Form
    {
        private Assembly _assembly;

        public AssemblyDataForm(string filename)
        {
            InitializeComponent();
                _assembly = Assembly.LoadFrom(filename);
                Text += " " + Path.GetFileName(filename);
                tbFullName.Text = _assembly.FullName;

                tbData.Text = "";
                Type[] types = _assembly.GetTypes();
                for (int i = 0; i < types.Length; i++)
                {
                    tbData.Text += types[i].Name + " " + types[i].MemberType.ToString() + Environment.NewLine;
                    MemberInfo[] infos = types[i].GetMembers();
                    for (int j = 0; j < infos.Length; j++)
                    {
                        tbData.Text += "\t" + infos[j].Name + " " + infos[j].MemberType.ToString() + Environment.NewLine;
                    }
                    tbData.Text += Environment.NewLine;
                }
            }
    }
}
