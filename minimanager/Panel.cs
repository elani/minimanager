﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MiniManager
{
    public partial class Panel : UserControl
    {
        private string _currentDirectory;
        private IFileSystemDataSource dataSource = new RemoteFileSystemDataSource();
        public string CurrentDirectory
        {
            get
            {
                return _currentDirectory;
            }
            set
            {
                _currentDirectory = value;
                GoToDirectory(_currentDirectory);
            }
        }
        // or use DirectoryInfo, can be usefull somewhere
        public FileSystemInfo[] SelectedItemsInfo
        {
            get
            {
                int count = lvPanel.SelectedItems.Count;
                FileSystemInfo[] items = new FileSystemInfo[count]; 
                for (int i = 0; i < lvPanel.SelectedItems.Count; i++)
                {
                    items[i] = (FileSystemInfo)lvPanel.SelectedItems[i].Tag;
                }
                return items;
            }
        }
        public Panel()
        {
            InitializeComponent();
        }
        public Panel(string directory)
        {
            try
            {
                InitializeComponent();
                cmbDisk.Items.AddRange(Environment.GetLogicalDrives());
                cmbDisk.Text = Path.GetPathRoot(directory);
                GoToDirectory(directory);
            }
            catch { }
        }
        private void GoToDirectory(string dir)
        {
            string oldDir = _currentDirectory;
            _currentDirectory = dir;
            try 
            {
                ShowDirectoryContent();
                btnBack.Enabled = dir != Path.GetPathRoot(dir);
                cmbDisk.Text = Path.GetPathRoot(dir);
                //fsWatcher.Path = _currentDirectory;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                GoToDirectory(oldDir);

            }
        }
        private void ShowDirectoryContent()
        {
            lvPanel.Items.Clear();
            ResponseDelegate descriptionDelegate = description => {
                Invoke((MethodInvoker)delegate 
                {
                    List<string> dirs = description.Directories;
                    List<string> files = description.Files;
                    for (int i = 0; i < dirs.Count; i++)
                    {
                        var item = new ListViewItem(dirs[i]);
                        var subItems = new ListViewItem.ListViewSubItem[] {
                            new ListViewItem.ListViewSubItem(item,"Directory")
                        };
                        
                        item.SubItems.AddRange(subItems);
                        lvPanel.Items.Add(item);
                        var info = new DirectoryInfo(Path.Combine(_currentDirectory, dirs[i]));
                        item.Tag = info;
                    }
                    for (int i = 0; i < files.Count; i++)
                    {
                        var item = new ListViewItem(files[i]);
                        var subItems = new ListViewItem.ListViewSubItem[] {
                        //new ListViewItem.ListViewSubItem(item, files[i].Extension)
                            new ListViewItem.ListViewSubItem(item, "File")
                        };
                        item.SubItems.AddRange(subItems);
                        lvPanel.Items.Add(item);
                        var info = new FileInfo(Path.Combine(_currentDirectory, files[i]));
                        item.Tag = info;
                    }
                });
                
            };

            dataSource.GetDirectoryContent(_currentDirectory, descriptionDelegate);

        }
        //private void ShowDirectoryContent()
        //{
        //        lvPanel.Items.Clear();
        //        var dInfo = new DirectoryInfo(_currentDirectory);
        //        DirectoryInfo[] dirs = dInfo.GetDirectories();
        //        for (int i = 0; i < dirs.Length; i++)
        //        {
        //            var item = new ListViewItem(dirs[i].Name);
        //            var subItems = new ListViewItem.ListViewSubItem[] {
        //            new ListViewItem.ListViewSubItem(item,"Directory")
        //            };
        //            item.SubItems.AddRange(subItems);
        //            lvPanel.Items.Add(item);
        //            item.Tag = dirs[i];
        //        }
        //        FileInfo[] files = dInfo.GetFiles();
        //        for (int i = 0; i < files.Length; i++)
        //        {
        //            var item = new ListViewItem(files[i].Name);
        //            var subItems = new ListViewItem.ListViewSubItem[] {
        //            new ListViewItem.ListViewSubItem(item, files[i].Extension)
        //        };
        //            item.SubItems.AddRange(subItems);
        //            lvPanel.Items.Add(item);
        //            item.Tag = files[i];
        //        }

        //}
        private void ChooseItem()
        {
            FileSystemInfo fsInfo = (FileSystemInfo)lvPanel.SelectedItems[0].Tag;
            if (fsInfo is DirectoryInfo)
            {
                GoToDirectory(fsInfo.FullName);
            }
        }
        public void ChangeSomeSettings()
        {
            //two methods changing font and forecolor
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            string root = Path.GetPathRoot(_currentDirectory);
            if (_currentDirectory != root)
            {
                GoToDirectory(Path.GetDirectoryName(_currentDirectory));
            }
        }

        private void lvPanel_DoubleClick(object sender, EventArgs e)
        {
            if (lvPanel.SelectedItems.Count != 0)
            {
                ChooseItem();
            }
        }

        private void cmbDisk_SelectedIndexChanged(object sender, EventArgs e)
        {
            //GoToDirectory(cmbDisk.SelectedItem.ToString());
        }

        public void SetFont()
        {
            lvPanel.Font = Settings.Instance.FontSettings;
        }
        public void SetColor()
        {
            lvPanel.ForeColor = Settings.Instance.ColorSettings;
        }

        private void Panel_Enter(object sender, EventArgs e)
        {
            PanelController.ActiveInstance.ActivePanel = this;
        }

        private void fsWatcher_Changed(object sender, FileSystemEventArgs e)
        {
            ShowDirectoryContent();
        }

        private void fsWatcher_Renamed(object sender, RenamedEventArgs e)
        {
            ShowDirectoryContent();
        }
    }
}
