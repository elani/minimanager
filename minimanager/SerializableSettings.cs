﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;
using System.ComponentModel;


namespace MiniManager
{
    public class SerializableSettings
    {
        //still don't know what to do here
        public string LeftPath
        {
            set;
            get;
        }
        public string RightPath
        {
            set;
            get;
        }
        public SerializableColor ForeColor
        {
            set;
            get;
        }
        public SerializableFont CustomFont
        {
            set;
            get;
        }

    }
}
