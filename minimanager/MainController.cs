﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniManager
{
    class MainController
    {
        private static  MainController _instance;
        public ManagerForm MainForm
        {
            get;
            set;
        }
        public PanelController PanelController
        { 
            get; 
            set;
        }
        public static MainController Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new MainController();
                }
                return _instance;
            }
        }
        protected MainController() 
        {
            try
            {
                MainForm = new ManagerForm();
                DeserializeSettings();
                PanelController = new PanelController(Settings.Instance.LeftPath, Settings.Instance.RightPath);
                PanelController.SetColor();
                PanelController.SetFont();
                MainForm.SetPanels(PanelController.Panels);
                bool leftPathEmpty = string.IsNullOrEmpty(Settings.Instance.LeftPath);
                bool rightPathEmpty = string.IsNullOrEmpty(Settings.Instance.RightPath);
                if (leftPathEmpty || rightPathEmpty)
                {
                    var dataSource = new RemoteFileSystemDataSource();
                    dataSource.GetRootPath(description =>
                    {
                        if (leftPathEmpty)
                        {

                            PanelController.LeftPanel.Invoke((MethodInvoker)delegate 
                            { 
                                PanelController.LeftPanel.CurrentDirectory = description.Path; 
                            });
                        }
                        if (rightPathEmpty)
                        {
                            PanelController.RightPanel.Invoke((MethodInvoker)delegate
                            {
                                PanelController.RightPanel.CurrentDirectory = description.Path;
                            });
                        }
                    });
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); //debug feature
            }
        }
        public void SetForm()
        {
            //set panels for MainForm
        }
        public void DeserializeSettings()
        {
            Settings.Instance.Deserialize();
        }
        public void SerializeSettings()
        {
            throw new NotImplementedException();
        }
    }
}
