﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace MiniManager
{
    static class FileSystemManager
    {
        static public void Copy(FileSystemInfo[] sourceItems, string targetPath)
        {
            if (sourceItems.Length != 0)
            {
                // Copy the files and overwrite destination files if they already exist.
                for (int i = 0; i < sourceItems.Length; i++)
                {
                    SingleCopy(sourceItems[i], targetPath);
                }
            }
            else
            {
                MessageBox.Show("No files selected");
            }
        }
        static private void CopyDir(string sourcePath, string targetPath)
        {
            string[] subDirectories = Directory.GetDirectories(sourcePath);
            string[] subFiles = Directory.GetFiles(sourcePath);
            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }
            for (int i = 0; i < subFiles.Length; i++)
            {
                string destFile = Path.Combine(targetPath, Path.GetFileName(subFiles[i]));
                File.Copy(subFiles[i], destFile, true);
            }
            for (int i = 0; i < subDirectories.Length; i++)
            {
                string destDir = Path.Combine(targetPath, Path.GetDirectoryName(subDirectories[i]));
                CopyDir(subDirectories[i], destDir);
            }
        }
        public async static Task CopyAsync(FileSystemInfo[] sourceItems, string targetPath)
        {
            await Task.Factory.StartNew(() =>
            {
                 FileSystemManager.Copy(sourceItems, targetPath);
            });
        }
        public async static Task MoveAsync(FileSystemInfo[] sourceItems, string targetPath)
        {
            await Task.Factory.StartNew(() =>
            {
                FileSystemManager.Move(sourceItems, targetPath);
            });
        }
        static public void Move(FileSystemInfo[] sourceItems, string targetPath)
        {
            if (sourceItems.Length != 0)
            {
                if (Path.GetPathRoot(sourceItems[0].FullName) == Path.GetPathRoot(targetPath))
                {
                    for (int i = 0; i < sourceItems.Length; i++)
                    {
                        SingleMove(sourceItems[i], targetPath);
                    }
                }
                else
                {
                    for (int i = 0; i < sourceItems.Length; i++)
                    {
                        SingleCopy(sourceItems[i], targetPath);
                        SingleRemove(sourceItems[i]);
                    }
                }
            }
            else
            {
                MessageBox.Show("No files selected");
            }
        }
        //public void/bool Remove/DeleteAsync() { //calls copy asynchronously }
        static public void Remove(FileSystemInfo[] sourceItems)
        {
            if (sourceItems.Length != 0)
            {
                for (int i = 0; i < sourceItems.Length; i++)
                {
                    SingleRemove(sourceItems[i]);
                }
            }
            else
            {
                MessageBox.Show("No files selected");
            }
        }
        static private void RemoveDir(string sourcePath)
        {
            string[] subDirectories = Directory.GetDirectories(sourcePath);
            string[] subFiles = Directory.GetFiles(sourcePath);
            for (int i = 0; i < subFiles.Length; i++)
            {
                File.Delete(subFiles[i]);
            }
            for (int i = 0; i < subDirectories.Length; i++)
            {
                RemoveDir(subDirectories[i]);
            }
            Directory.Delete(sourcePath);
        }
        static private void SingleCopy(FileSystemInfo sourceItem, string targetPath)
        {
            try
            {
                if (sourceItem is FileInfo)
                {
                    FileInfo fInfo = (FileInfo)sourceItem;
                    // Use static Path methods to extract only the file name from the path.
                    string fileName = sourceItem.FullName;
                    string destFile = Path.Combine(targetPath, fInfo.Name);
                    File.Copy(fileName, destFile, true);
                }
                else if (sourceItem is DirectoryInfo)
                {
                    DirectoryInfo dInfo = (DirectoryInfo)sourceItem;
                    string destDir = Path.Combine(targetPath, dInfo.Name);
                    CopyDir(sourceItem.FullName, destDir);
                }
                else
                {
                    throw new Exception("Selected item is neither file, nor directory");
                }
            }
            catch
            {
                MessageBox.Show("File or directory could not be changed, while it is used in another operation");
            }
        }
        static private void SingleRemove(FileSystemInfo sourceItem)
        {
            try
            {
                if (sourceItem is FileInfo)
                {
                    File.Delete(sourceItem.FullName);
                }
                else if (sourceItem is DirectoryInfo)
                {
                    RemoveDir(sourceItem.FullName);
                }
                else
                {
                    throw new Exception("Selected item is neither file, nor directory");
                }
            }
            catch
            {
                MessageBox.Show("File or directory could not be changed, while it is used in another operation");
            }
        }
        static private void SingleMove(FileSystemInfo sourceItem, string targetPath)
        {
            try
            {
                if (sourceItem is FileInfo)
                {
                    try
                    {
                        FileInfo fInfo = (FileInfo)sourceItem;
                        // Use static Path methods to extract only the file name from the path.
                        string fileName = sourceItem.FullName;
                        string destFile = Path.Combine(targetPath, fInfo.Name);
                        File.Move(fileName, destFile);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("File with this name already exists: " + sourceItem.Name);
                    }
                }
                else if (sourceItem is DirectoryInfo)
                {
                    DirectoryInfo dInfo = (DirectoryInfo)sourceItem;
                    string destDir = Path.Combine(targetPath, dInfo.Name);
                    Directory.Move(sourceItem.FullName, destDir);
                }
                else
                {
                    throw new Exception("Selected item is neither file, nor directory");
                }
            }
            catch
            {
                MessageBox.Show("File or directory could not be changed, while it is used in another operation");
            }
        }
        static public void Rename(FileSystemInfo[] sourceItem, string name)
        {
            try
            {
                if (sourceItem.Length > 1)
                {
                    throw new Exception("Too many files selected");
                }
                else
                {
                    string fullName = sourceItem[0].FullName;
                    string newName = Path.Combine(Path.GetDirectoryName(fullName), name);
                    if (sourceItem[0] is FileInfo)
                    {
                        File.Move(fullName, newName);
                    }
                    else if (sourceItem[0] is DirectoryInfo)
                    {
                        Directory.Move(fullName, newName);
                    }
                    else
                    {
                        throw new Exception("Selected item is neither file, nor directory");
                    }
                }
            }
            catch
            {
                MessageBox.Show("File or directory could not be changed, while it is used in another operation");
            }
            }
        }
        
}

