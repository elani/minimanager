﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace MiniManager
{
    
    public class RemoteFileSystemDataSource: IFileSystemDataSource
    {
        private RestClient client = new RestClient("http://localhost:8000/");
        public void GetDirectoryContent(string path, ResponseDelegate description)
        {
            var request = new RestRequest("/directory_info", Method.POST);
            request.AddJsonBody(path);
            var asyncHandle = client.ExecuteAsync<DirectoryDescription>(request, response =>
            {
                DirectoryDescription dir = response.Data;
                if (dir is DirectoryDescription)
                {
                    description(dir);
                }
            });
        }

        public void GetRootPath(ResponseDelegate description)
        {
            var request = new RestRequest("/root_directory", Method.GET);
            var asyncHandle = client.ExecuteAsync<DirectoryDescription>(request, response => {
                DirectoryDescription dir = response.Data;
                if (dir is DirectoryDescription)
                {
                    description(dir);
                }
            });
        }
    }
}
