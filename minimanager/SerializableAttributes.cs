﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Xml.Serialization;
using System.ComponentModel;

namespace MiniManager
{
    public class SerializableColor
    {
        public SerializableColor()
        {
            ColorValue = Color.Black;
        }

        public SerializableColor(Color color)
        {
            ColorValue = color;
        }

        [XmlIgnore]
        public Color ColorValue { get; set; }
        [XmlElement("ColorValue")]
        public int ColorAsArgb
        {
            get 
            { 
                return ColorValue.ToArgb(); 
            }
            set 
            { 
                ColorValue = Color.FromArgb(value); 
            }
        }

        public static implicit operator Color(SerializableColor serializableColor)
        {
            if (serializableColor == null) {
                return Color.Black;
            }
            return serializableColor.ColorValue;
        }
        public static implicit operator SerializableColor(Color color)
        {
            return new SerializableColor(color);
        }
    }

    public class SerializableFont
    {
        public SerializableFont()
        {
            FontValue = null;
        }

        public SerializableFont(Font font)
        {
            FontValue = font;
        }

        [XmlIgnore]
        public Font FontValue { get; set; }             //сам шрифт мы игнорим 

        [XmlElement("FontValue")]                       //вместо него в хмл запихиваем под именем FontValue стрингу
        public string SerializeFontAttribute
        {
            get
            {
                return FontXmlConverter.ConvertToString(FontValue);
            }
            set
            {
                FontValue = FontXmlConverter.ConvertToFont(value);
            }
        }

        public static implicit operator Font(SerializableFont serializeableFont)    // такая конструкция используется для переопределения неявного приведения 
        //типа серилайзабл фонт к обычному фонту. то есть этот метод произойдет 
        //при присвоении фонту сериалайзабл фонта или при передаче функции вместо фонта сериалайзабл фонта
        {
            if (serializeableFont == null)
                return null;
            return serializeableFont.FontValue;
        }

        public static implicit operator SerializableFont(Font font) //аналогично неявное приведение типов только в обратную сторону
        {
            return new SerializableFont(font);
        }
    }

    public static class FontXmlConverter //тупо класс, который конвертит фонт в стрингу и обратно
    {
        public static string ConvertToString(Font font)
        {
            try
            {
                if (font != null)
                {
                    TypeConverter converter = TypeDescriptor.GetConverter(typeof(Font));    //тут некая магия с рефлексией, видимо
                    return converter.ConvertToString(font);
                }
                else
                    return null;
            }
            catch { System.Diagnostics.Debug.WriteLine("Unable to convert"); }
            return null;
        }
        public static Font ConvertToFont(string fontString)
        {
            try
            {
                TypeConverter converter = TypeDescriptor.GetConverter(typeof(Font));    //и тут
                return (Font)converter.ConvertFromString(fontString);
            }
            catch { System.Diagnostics.Debug.WriteLine("Unable to convert"); }
            return null;
        }
    }
}
