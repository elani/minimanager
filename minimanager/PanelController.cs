﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MiniManager
{
    class PanelController
    {
        /// <summary>
        /// Gets panelController that was created in MainController
        /// </summary>
        public static PanelController ActiveInstance
        {
            get
            {
                return MainController.Instance.PanelController;
            }
        }
        private Panel _leftPanel, _rightPanel;
        private Panel _activePanel, _inactivePanel;
        public Panel LeftPanel
        {
            get { return _leftPanel; }
        }
        public Panel RightPanel
        {
            get { return _rightPanel; }
        }
        public Panel[] Panels
        {
            get { return _panels; }
        }
        //can be useful someday for active/unactive properties/methods
        private Panel[] _panels;
        //конструктор с определением путей к папкам
        public PanelController(string leftPath,string rightPath)
        {
            _leftPanel = new Panel(leftPath);
            _rightPanel = new Panel(rightPath);
            _panels = new Panel[] { _leftPanel, _rightPanel };
        }

        public void ChangeSomeSettingsInPanels()
        {
            //two methods for font and forecolor
            _leftPanel.ChangeSomeSettings();
            _rightPanel.ChangeSomeSettings();
        }

        public Panel ActivePanel
        {
            get
            {
                return _activePanel;
            }
            set
            {
                if (Panels != null)
                {
                    if (Panels[0] == value)
                    {
                        _inactivePanel = Panels[1];
                    }
                    else
                    {
                        _inactivePanel = Panels[0];
                    }
                }
                _activePanel = value;
            }
        }
        public Panel InactivePanel
        {
            get
            {
               return _inactivePanel;
            }
            set
            {
                if (Panels != null)
                {
                    if (Panels[0] == value)
                    {
                        _activePanel = Panels[1];
                    }
                    else
                    {
                        _activePanel = Panels[0];
                    }
                }
                _inactivePanel = value;
            }
        }
        public void SetColor()
        {
            _leftPanel.SetColor();
            _rightPanel.SetColor();
        }
        public void SetFont()
        {
            _leftPanel.SetFont();
            _rightPanel.SetFont();
        }
        public async void CopyItems()
        {
            await FileSystemManager.CopyAsync(ActivePanel.SelectedItemsInfo, InactivePanel.CurrentDirectory);

        }
        public void RemoveItems()
        {
            FileSystemManager.Remove(ActivePanel.SelectedItemsInfo);
        }
        public async void MoveItems()
        {
            await FileSystemManager.MoveAsync(ActivePanel.SelectedItemsInfo, InactivePanel.CurrentDirectory);
        }
        public void RenameItems()
        {
            FileSystemInfo[] fInfos = ActivePanel.SelectedItemsInfo;
            if (fInfos.Length > 0)
            {
                var dialog = new RenameDialog();
                dialog.FileName = fInfos[0].Name;
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    FileSystemManager.Rename(fInfos, dialog.FileName);
                }
            } 
            //FileSystemManager.Rename(ActivePanel.SelectedItemsInfo, InactivePanel.CurrentDirectory);
        }
    }
}
