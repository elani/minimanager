﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Windows.Forms;

namespace MiniManager
{
    class Settings
    {
        private static Settings _instance;
        [XmlIgnore]
        public static Settings Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Settings();
                }
                return _instance;
            }
        }
        private Font _fontSettings;
        private Color _colorSettings;
        private string _leftPath;
        private string _rightPath;
        public Font FontSettings
        {
            get { return _fontSettings; }
            set 
            {
                _fontSettings = value;
                PanelController.ActiveInstance.SetFont();
            }
        }
        public Color ColorSettings
        {
            get { return _colorSettings; }
            set
            {
                _colorSettings = value;
                PanelController.ActiveInstance.SetColor();
            }
        }
        //public properties with setters, that call PanelController UI methods
        public string LeftPath
        {
            get
            {
                //panelsController.LeftPanel.Current...
                return _leftPath;
            }
        }
        public string RightPath
        {
            get
            {
                return _rightPath;
            }
        }
        protected Settings()
        {
            //initializing properties with default values
            _fontSettings = SystemFonts.DefaultFont;
            _colorSettings = Color.Black;
            //_leftPath = "C:\\";
            //_rightPath = "C:\\";

        }
        private string _xmlPath = Path.Combine(Directory.GetCurrentDirectory(), "config.xml");

        public void Serialize()
        {
            _leftPath = PanelController.ActiveInstance.LeftPanel.CurrentDirectory;
            _rightPath = PanelController.ActiveInstance.RightPanel.CurrentDirectory;
            var serSet = new SerializableSettings
            {
                LeftPath = this.LeftPath,
                RightPath = this.RightPath,
                ForeColor = this.ColorSettings,
                CustomFont = this.FontSettings
            };
            var serializer = new XmlSerializer(typeof(SerializableSettings));
            var fStream = new FileStream(_xmlPath, FileMode.Create, FileAccess.Write);
            serializer.Serialize(fStream, serSet);
            fStream.Close();
        }
        public void Deserialize()
        {
            var serializer = new XmlSerializer(typeof(SerializableSettings));
            var fStream = new FileStream(_xmlPath, FileMode.OpenOrCreate, FileAccess.Read);
            if (fStream.Length != 0)
            {
                try
                {
                    SerializableSettings desSet = (SerializableSettings)serializer.Deserialize(fStream);
                    _rightPath = desSet.RightPath;
                    _leftPath = desSet.LeftPath;
                    _fontSettings = desSet.CustomFont;
                    _colorSettings = desSet.ForeColor;
                    fStream.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.InnerException.Message);
                }
            }
        }
    }
}
